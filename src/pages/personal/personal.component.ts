import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-personal',
  templateUrl: 'personal.component.html'
})
export class PersonalPage {

  constructor(public navCtrl: NavController) {

  }

}